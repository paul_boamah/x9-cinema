import 'package:flutter/material.dart';

const Color blackColor = Color.fromRGBO(0, 0, 0, 1);
const Color redColors = Color.fromRGBO(253, 10, 76, 1);
const Color greyColor = Color.fromRGBO(181, 188, 200, 1);
const Color whiteColor = Color.fromRGBO(245, 247, 249, 1);
