import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:movie_app/themes.dart';

import 'details_page.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        scaffoldBackgroundColor: whiteColor,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final List<String> brands = [
    'Recommended',
    'Netflix',
    'HBOGO',
    'Amazon',
    'Microsoft',
  ];

  @override
  Widget build(BuildContext context) {
    ThemeData _theme = Theme.of(context);

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: _theme.scaffoldBackgroundColor,
        title: RichText(
          text: TextSpan(
              style: TextStyle(
                color: blackColor,
                fontSize: 20,
              ),
              children: [
                TextSpan(
                  text: 'X9',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                TextSpan(text: 'CINEMA')
              ]),
        ),
        actions: <Widget>[
          IconButton(
            color: blackColor,
            icon: Icon(Icons.search),
            onPressed: () {},
          ),
          IconButton(
            color: blackColor,
            icon: Icon(Icons.menu),
            onPressed: () {},
          )
        ],
      ),
      body: Stack(
        children: <Widget>[
          ListView(
            children: <Widget>[
              Container(
                height: 65,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: brands
                      .map(
                        (e) => Container(
                          padding:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                          margin: EdgeInsets.symmetric(vertical: 5),
                          child: Container(
                            // decoration: BoxDecoration(boxShadow: [
                            //   BoxShadow(
                            //     blurRadius: 1,
                            //     color: redColors.withOpacity(0.05),
                            //     offset: Offset(0, 40.0),
                            //     spreadRadius: 0,
                            //   )
                            // ]),
                            child: MaterialButton(
                              elevation: e == 'Recommended' ? 5 : 0,
                              color:
                                  e == 'Recommended' ? redColors : whiteColor,
                              padding: EdgeInsets.symmetric(
                                  horizontal: 25, vertical: 10),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(17)),
                              textColor:
                                  e == 'Recommended' ? whiteColor : blackColor,
                              child: Text(e),
                              onPressed: () {},
                            ),
                          ),
                        ),
                      )
                      .toList(),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 20, left: 20, right: 20, bottom: 5),
                child: Text(
                  'Movies',
                  style: _theme.textTheme.bodyText2.copyWith(
                    color: blackColor,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                height: 325,
                child: ListView(
                  padding: EdgeInsets.symmetric(horizontal: 15),
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[
                    buildCard(
                      context: context,
                      title: 'Drive',
                      subTitle: 'Ryan Guslying',
                      url: 'images/pic1.jpg',
                    ),
                    buildCard(
                      context: context,
                      title: 'Mad Max',
                      subTitle: 'Ryan Guslying',
                      url: 'images/pic2.jpg',
                    ),
                    buildCard(
                      context: context,
                      title: 'Thor',
                      subTitle: 'Ryan Guslying',
                      url: 'images/thor.jpg',
                    ),
                  ],
                ),
                // child: ListView.builder(itemBuilder: null),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 20, left: 20, right: 20, bottom: 5),
                child: Text(
                  'Series',
                  style: _theme.textTheme.bodyText2.copyWith(
                    color: blackColor,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Container(
                height: 325,
                child: ListView(
                  padding: EdgeInsets.symmetric(horizontal: 15),
                  scrollDirection: Axis.horizontal,
                  children: <Widget>[
                    buildCard(
                      context: context,
                      title: 'Thor',
                      subTitle: 'Ryan Guslying',
                      url: 'images/thor.jpg',
                    ),
                    buildCard(
                      context: context,
                      title: 'The Flash',
                      subTitle: 'Ryan Guslying',
                      url: 'images/flash.jpg',
                    ),
                  ],
                ),
                // child: ListView.builder(itemBuilder: null),
              ),
              SizedBox(height: 70),
            ],
          ),
          buildbottomBar()
        ],
      ),
    );
  }

  Positioned buildbottomBar() {
    return Positioned(
      bottom: 0,
      left: 0,
      right: 0,
      child: Card(
        margin: EdgeInsets.zero,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(15),
            topRight: Radius.circular(15),
          ),
        ),
        child: Container(
          height: 50,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              IconButton(
                icon: Image(
                  image: AssetImage('images/film.png'),
                  height: 25,
                  width: 25,
                  color: redColors,
                ),
                onPressed: () {},
              ),
              IconButton(
                icon: Image(
                    image: AssetImage('images/fire.png'),
                    height: 20,
                    width: 20,
                    color: greyColor),
                onPressed: () {},
              ),
              IconButton(
                icon: Icon(Icons.bookmark, color: greyColor),
                onPressed: () {},
              ),
              IconButton(
                icon: Icon(Icons.person, color: greyColor),
                onPressed: () {},
              ),
            ],
          ),
        ),
      ),
    );
  }

  Card buildCard(
      {BuildContext context, String url, String title, String subTitle}) {
    ThemeData _theme = Theme.of(context);

    return Card(
      elevation: 5,
      margin: EdgeInsets.all(10),
      clipBehavior: Clip.hardEdge,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(10),
          topRight: Radius.circular(25),
          bottomLeft: Radius.circular(25),
          bottomRight: Radius.circular(25),
        ),
      ),
      child: InkWell(
        onTap: () {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (_) => DetailsPage()));
        },
        child: Container(
          width: 210,
          decoration: BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.cover,
              image: AssetImage(url),
            ),
          ),
          child: Container(
            margin: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Spacer(),
                Text(
                  title.toUpperCase(),
                  style: _theme.textTheme.headline5.copyWith(
                    fontSize: 32,
                    fontWeight: FontWeight.w700,
                    color: whiteColor,
                  ),
                ),
                Text(
                  subTitle,
                  style: _theme.textTheme.headline5.copyWith(
                    fontWeight: FontWeight.w500,
                    fontSize: 20,
                    color: whiteColor,
                  ),
                ),
                SizedBox(height: 5),
                buildStarsRow()
              ],
            ),
          ),
        ),
      ),
    );
  }
}

Row buildStarsRow() {
  return Row(
    children: <Widget>[
      Icon(
        Icons.star,
        color: whiteColor,
      ),
      Icon(
        Icons.star,
        color: whiteColor,
      ),
      Icon(
        Icons.star,
        color: whiteColor,
      ),
      Icon(
        Icons.star_half,
        color: whiteColor,
      ),
      Icon(
        Icons.star_border,
        color: whiteColor,
      ),
      Spacer(),
      Icon(
        Icons.bookmark,
        color: whiteColor,
      )
    ],
  );
}

class MOVIE {
  String image;
  String title;
  String subtitle;

  MOVIE({
    this.image,
    this.title,
    this.subtitle,
  });
}
