import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:movie_app/themes.dart';
import './main.dart';

class DetailsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ThemeData _theme = Theme.of(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: _theme.scaffoldBackgroundColor,
        elevation: 0,
        leading: IconButton(
          color: blackColor,
          icon: Icon(Icons.arrow_back_ios),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: RichText(
          text: TextSpan(
            style: TextStyle(
              color: blackColor,
              fontSize: 20,
            ),
            children: [
              TextSpan(
                text: 'MAD-MAX - ',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
              ),
              TextSpan(text: 'FURY ROAD'),
            ],
          ),
        ),
      ),
      body: ListView(
        padding: EdgeInsets.all(20).copyWith(
          top: 10,
        ),
        children: <Widget>[
          Container(
            child: Stack(
              children: <Widget>[
                Container(
                  height: 300,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(20),
                      topLeft: Radius.circular(20),
                    ),
                    image: DecorationImage(
                      fit: BoxFit.cover,
                      image: AssetImage('images/pic2.jpg'),
                    ),
                  ),
                ),
                Positioned(
                  right: 0,
                  top: 0,
                  bottom: 0,
                  child: Container(
                    height: 250,
                    width: 70,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(20),
                        topLeft: Radius.circular(20),
                      ),
                    ),
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text(
                          '1h 58m',
                          style: _theme.textTheme.caption.copyWith(
                            color: blackColor,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Divider(
                          color: greyColor,
                          thickness: 1.2,
                        ),
                        Text(
                          'Action',
                          style: _theme.textTheme.caption.copyWith(
                            color: blackColor,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Divider(
                          color: greyColor,
                          thickness: 1.2,
                        ),
                        Text(
                          '4,5',
                          style: _theme.textTheme.caption.copyWith(
                            color: blackColor,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Divider(
                          color: greyColor,
                          thickness: 1.2,
                        ),
                        Icon(Icons.bookmark)
                      ],
                    ),
                  ),
                ),
                Positioned(
                  bottom: 20,
                  left: 20,
                  child: Container(height: 30, child: buildStarsRow()),
                ),
                Positioned(
                    top: 20,
                    left: 20,
                    child: Container(
                      height: 30,
                      child: Row(
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                                color: whiteColor,
                                borderRadius: BorderRadius.circular(4)),
                            width: 50,
                            height: 4,
                          ),
                          SizedBox(width: 8),
                          Container(
                            decoration: BoxDecoration(
                                color: whiteColor,
                                borderRadius: BorderRadius.circular(4)),
                            width: 4,
                            height: 4,
                          ),
                          SizedBox(width: 8),
                          Container(
                            decoration: BoxDecoration(
                                color: whiteColor,
                                borderRadius: BorderRadius.circular(4)),
                            width: 4,
                            height: 4,
                          ),
                        ],
                      ),
                    ))
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 15),
            child: Text(
              'Synopsis',
              style: _theme.textTheme.bodyText2.copyWith(
                color: blackColor,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 0),
            child: Text(
              'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Pariatur fuga eius veniam eos modi! Consequatur, vitae vel magnam obcaecati hic ratione quas omnis minus quidem, veniam nesciunt, praesentium aperiam doloremque!',
              style: GoogleFonts.raleway(
                  textStyle: _theme.textTheme.bodyText1.copyWith(
                fontSize: 12,
                height: 1.8,
                color: Colors.grey,
              )),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 15),
            child: Text(
              'Casts',
              style: _theme.textTheme.bodyText1.copyWith(
                color: blackColor,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Container(
            height: 100,
            child: ListView.builder(
              itemCount: actors.length,
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) => Container(
                height: 80,
                width: 90,
                margin: EdgeInsets.all(10),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: AssetImage(actors[index]),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

List<String> actors = [
  'images/mainpic1.jpg',
  'images/mainpic2.jpg',
  'images/mainpic3.jpg',
  'images/mainpic4.jpg',
];
